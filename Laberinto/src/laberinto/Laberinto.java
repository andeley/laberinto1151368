/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laberinto;

import java.net.*;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Angie
 */
public class Laberinto {

    /**
     * @param args the command line arguments
     */
    static short[][] laberinto;

    /**
     * Permite crear un objeto de tipo ArchivoLeerURL especificando su URL
     *
     * @param direccion la URL donde está el archivo
     */
    public Laberinto(String url) {
        ArchivoLeerURL leer = new ArchivoLeerURL(url);
        Object[] lines = leer.leerArchivo();
        String[] can = lines[0].toString().split(";");
        int len = lines.length;
        int c = can.length;
        this.laberinto = new short[len][c];

        for (int i = 0; i < lines.length; i++) {
//                if(nue.get(i)!=";"){
            String[] col = lines[i].toString().split(";");
            for (int j = 0; j < c; j++) {
                this.laberinto[i][j] = Short.parseShort(col[j]);
            }
//                }

        }

    }

    public Laberinto() {
    }

    /**
     * Método que retorna el archivo en un vector de Object. Cada línea se
     * almacena secuencialmente en cada posición del vector
     *
     * @return un vector con el archivo almacenado en la URL
     */
  

    public static String getCamino(int posxThe, int posyThes, int posxMin, int posyMin, int posxSalida, int posySalida) { // recibe posicion de theseus, minutauro y la salida del laberinto
        
        int ady [][] = new int [laberinto.length][laberinto[0].length];
	int [][] q =new int [laberinto.length+1][2];
	long dist [][]  = new long [laberinto.length][laberinto[0].length];
        int puntI=0;
        int punF = laberinto.length;
	for(int i =0; i<laberinto.length; i++){
		for(int j = 0; j<laberinto[i].length; j++){
			
			if(laberinto[posxThe][posyThes]==9){
				//agregar a q el nodo inicial que va a visitar el bfs
//				int [] primero = new int [2];
//				primero[0]= i;
//				primero[1]= j;
                                punF++;
				q[punF][0]= i;
                                q[punF][1]= j;
                               
                                
                                
//                                pos--;
				//marcar dist en la coordenada inicial como 0-> es la direccion inicial
				dist[i][j] = 0;
			}else{
				dist[i][j] = -1;
			}
                         ady[i][j]= laberinto[i][j];
//			 System.out.print( dist[i][j]+ " ");
		}
//                System.out.println("");
	}
 
	encontrarCamino(ady, q, 0,0, dist, laberinto.length, laberinto[0].length);
              
		for(int i =0; i<laberinto.length; i++){
		for(int j = 0; j<laberinto[i].length; j++){
                        laberinto[i][j] = Short.parseShort(dist[i][j]+"");
			return dist[i][j]+ " ";
		}
			
		}

        return dist + " ";

    }

   private static void encontrarCamino(int ady[][], int [][] q, int puntI, int puntF, long dist[][], int filas, int columnas){
        int dy[] = {1,0,-1, 0}; //Posibles movimientos:
	int dx[] = {0,1, 0,-1};
        
        if(q!=null){
            int u[]= new int[2];
            u[0]=q[puntI][0];
            u[1]=q[puntI][1];
//            int [] u = q[puntI]; // aqui va el primer elemento que entró
          //  q[puntI][0]= q[puntI+1][0];
            puntI++;
            
//            
            
            for(int i = 0; i<4; i++){
 
            if (u[1]+dy[i] >= 0 && u[1]+dy[i] < columnas && u[0]+dx[i] >= 0 && u[0]+dx[i] < filas){
                if(dist[u[0]+dx[i]][u[1]+dy[i]]==-1 && ady[u[0]+dx[i]][u[1]+dy[i]]!=-1){
				
				
                                puntF++;
				q[puntF][0]=u[0]+dx[i];
                                q[puntF][1]=u[1]+dy[i];
                                
                                dist[u[0]+dx[i]][u[1]+dy[i]]= dist[u[0]][u[1]] + 1;
			}
	
 
            }
        }
            encontrarCamino(ady, q,puntI,puntF, dist, filas, columnas);
        }
        
    
    
    }
 

}
